function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
      <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
        <div className="carousel-inner">
        <div className="carousel-item active">
          <a href= "http://localhost:3000/automobiles" >
            <img src="https://images.pexels.com/photos/2739286/pexels-photo-2739286.jpeg?auto=compress&cs=tinysrgb&w=800" className="d-block w-100" alt="pink vintage car with hood open and headlights" />
          </a>
          <div className="carousel-caption d-none d-md-block">
            <h1 className="carousel_header">Track Inventory</h1>
            <p>Add automobiles to the inventory and view a list of all automobiles that are currently in inventory</p>
        </div>
        </div>
        <div className="carousel-item">
          <a href = "http://localhost:3000/appointments">
            <img src="https://images.pexels.com/photos/1768826/pexels-photo-1768826.jpeg?auto=compress&cs=tinysrgb&w=800" className="d-block w-100" alt="side of dark green car parked infront of greenery" />
          </a>
          <div className="carousel-caption d-none d-md-block">
            <h1 className="carousel_header">Manage Service Appointments</h1>
            <p>Add appointments, view upcoming appointments, mark appointments as cancelled or complete</p>
          </div>
        </div>
        <div className="carousel-item">
          <a href = "http://localhost:3000/sales/">
            <img src="https://images.pexels.com/photos/1974520/pexels-photo-1974520.jpeg?auto=compress&cs=tinysrgb&w=1600" className="d-block w-100" alt="whit old fashioned cadillac with palm trees in the background" />
          </a>
          <div className="carousel-caption d-none d-md-block">
          <h1 className="carousel_header">Track Sales History</h1>
          <p>Record new sales, view a history of all sales or filter history by employee number to see the sales made by an individual salesperson</p>
        </div>
        </div>
      </div>
      <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Previous</span>
      </button>
      <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Next</span>
      </button>
    </div>
  </div>

  );
}
export default MainPage;
