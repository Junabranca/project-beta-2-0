import React, { useState, useEffect} from "react";
import { Link } from 'react-router-dom';


function CustomerList() {
    const [customers, setCustomers] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setCustomers(data.customers)
        }
      }

      useEffect(() => {
          fetchData();
      }, []);



    return(
        <div>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">Phone Number</th>
                    </tr>
                </thead>
                <tbody>
                    {customers?.map(customer => {
                        return (
                            <tr key={customer.phone_number}>
                                <td>{customer.name}</td>
                                <td>{customer.address}</td>
                                <td>{customer.phone_number}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className="col text-center">
                <Link to="/customers/new" className="btn btn-outline-primary btn-sm px-4 gap-3">Add a potential customer</Link>
            </div>
        </div>
      );
    }

export default CustomerList;
