import React, { useState, useEffect} from "react";
import { Link } from 'react-router-dom';


function TechniciansList() {
    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setTechnicians(data.technicians)
        }
      }

      useEffect(() => {
          fetchData();
      }, []);



    return(
        <div>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Employee Number</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians?.map(technician => {
                        return (
                            <tr key={technician.employee_number}>
                                <td>{technician.technician_name}</td>
                                <td>{technician.employee_number}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className="col text-center">
                <Link to="/technicians/new" className="btn btn-outline-primary btn-sm px-4 gap-3">Add a new technician</Link>
            </div>
        </div>
      );
    }

export default TechniciansList;
